**Gulp4 main build**
to run you need to install node.js 18.12.1 version

**Run commands:**
npm install - install modules
npm start - start the dev
npm start - start the dev

**Job Description:**

Finished files are collected in the build folder

Folder structure in src:

- assets - all auxiliary files

    - fonts - font files
    - images - pictures
        - content - will be loaded from the admin panel in the future
        - static - pictures wired in the code
        - sprite - pictures assembled into a sprite
            -svg
            -png

    - js - scripts
        - components - project skips
            - there can be files with any names according to the project structure (swipers.js, forms.js, etc.)
        - plugins - plugins to be connected (there is a connection sample from the modules)
            - you can connect plugins files
        - separate - scripts that should remain separate files after compilation
            -jquery.min.js

    - styles - styles
        - tools - tools (animations, variables, mixins)
        - vendors - inclusions (fonts, normalize, reset, plug-in styles of modules and plugins)
        - settings - basic settings (colors, global settings, media variables)
        - layout - size settings (containers, rows, grids)
        - elements - small repeating elements (buttons, links) - file name = name of the class that it contains
        - components - repeating blocks (all components) - file name = name of the class that it contains
        - patterns - non-repetitive blocks (e.g. page settings) - file name = class name that it contains
        style.scss - all folders are connected here in the prescribed order

        _blank - a blank template for breaking down each component into even more settings - not used in standard projects

- html
     - folders with repeating blocks
     - the pages themselves

- php - folder is created to create php files in the process of setting up the administration panel
     - folders with repeating blocks
     - the pages themselves

Note on class names accepted in true.code:

The name of the classes remains the choice of a particular specialist, but must correspond to the English name. It is unacceptable to use Russian transliterated words in class names.
Names are written in Latin in lower case.
A hyphen (-) is used to separate words in names.
The block name specifies the namespace for its elements and modifiers.
The element name is separated from the block name by two underscores (__).
The modifier name is separated from the block or element name by a double hyphen (--).
Naming example:
“class” - the class itself
"class class--modificator” - the class with the modifier
“class__child” - child class
“class__child class__child--modificator” - child class with modifier

Writing styles for classes in an assembly:

```
.class {
     styles for the main class

     &--modifier {
         modifier styles
     }

     &--second-modifier {
         styles for the second modifier
     }
}

.class_child {
     styles for child class

     &--modifier {
         styles for child class modifier
     }
}
```

Media expressions in each file below for all classes

```
@media screen and (max-width: map-get($tablet, lg) - 1) {

     .class {
         styles for the main class

         &--modifier {
             modifier styles
         }
     }

     .class_child {
         styles for child class

         &--modifier {
             styles for child class modifier
         }
     }
}
```

